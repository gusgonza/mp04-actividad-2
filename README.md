# Actividad 2 - Formulario de Inscripción para Grado Superior

Este proyecto simula un formulario de inscripción para un grado superior, donde se recopilan los datos personales del solicitante y los documentos adjuntos necesarios.

## Requisitos Implementados

### Datos Personales:
- Todos los campos son obligatorios.
- El DNI debe tener 8 dígitos seguidos de una letra al final (mayúscula o minúscula). Las letras I, L, Ñ y U (y sus respectivas minúsculas) no son permitidas.
- El código postal debe contener 5 dígitos.
- La fecha de nacimiento debe estar comprendida entre "1900-01-01" y "01-01-2023".
- La provincia debe mostrar todas las capitales de España.

### Documentos Adjuntos:
- El botón para certificados académicos solo admite 1 archivo.
- El botón para otros documentos admite múltiples archivos.

## Contenido del Repositorio

- **index.html:** Archivo HTML principal del proyecto.
- **styles.css:** Archivo CSS que contiene los estilos utilizados en el proyecto.

## Restricciones

- Todos los campos son obligatorios.
- El DNI debe tener 8 dígitos seguidos de una letra al final (mayúscula o minúscula). Las letras I, L, Ñ y U (y sus respectivas minúsculas) no son permitidas.
- El código postal debe contener 5 dígitos.
- La fecha de nacimiento debe estar comprendida entre "1900-01-01" y "01-01-2023".
- La provincia debe mostrar todas las capitales de España.
- El botón de certificados académicos solo admite 1 archivo.
- El botón de otros documentos admite múltiples archivos.


[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=success)](https://gitlab.com/gusgonza/mp04-actividad-2/-/tree/main)
